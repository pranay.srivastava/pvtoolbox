# pvtoolbox

To use, add the following snippet to your dockerfile:

**X86_64 (static binary)**
```
FROM registry.gitlab.com/pantavor/pantavisor-runtime/pvtoolbox:X86_64-master as pvtoolbox

COPY --chown=0:0 --from pvtoolbox /usr/local/bin/pvsocket /usr/local/bin/pvsocket
COPY --chown=0:0 --from pvtoolbox /usr/local/bin/pvlog /usr/local/bin/pvlog
COPY --chown=0:0 --from pvtoolbox /usr/local/bin/pvmeta /usr/local/bin/pvmeta

RUN chmod +x /usr/local/bin/pvsocket
RUN chmod +x /usr/local/bin/pvlog
RUN chmod +x /usr/local/bin/pvmeta
```

**ARM 32-bit (static binary)**
```
FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvtoolbox:ARM32V6-master as pvsocket

COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvsocket /usr/local/bin/pvsocket
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvlog /usr/local/bin/pvlog
COPY --chown=0:0 --from=pvtoolbox /usr/local/bin/pvmeta /usr/local/bin/pvmeta

RUN chmod +x /usr/local/bin/pvsocket
RUN chmod +x /usr/local/bin/pvlog
RUN chmod +x /usr/local/bin/pvmeta
```
